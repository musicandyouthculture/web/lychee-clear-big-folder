<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 21.02.2019
 * Time: 14:26
 */

namespace ClearBigFolder;

use SplObserver;
use SplSubject;

/**
 * Class ClearBigFolder
 *
 * @link https://github.com/electerious/Lychee/blob/master/docs/Plugins.md
 *
 * @package ClearBigFolder
 */
class ClearBigFolder implements SplObserver
{
    public function __construct()
    {
        return true;
    }

    public function update(SplSubject $subject)
    {
        if ($subject->action !== 'Photo:add:after') {
            return false;
        }

        // filter before-hand for specific image files
        // so we don't accidentally delete something else
        $imageFiles = scandir(LYCHEE_UPLOADS_BIG);
        $imageFiles = array_filter($imageFiles, function ($file) {
            if (in_array($file, ['.', '..'])) {
                return false;
            }

            return preg_match('/(jpe?g|png)$/i', $file);
        });

        // remove them
        foreach ($imageFiles as $file) {
            if (! unlink(LYCHEE_UPLOADS_BIG . $file)) {
                // print and log error if unsuccessful
                print_r(error_get_last());
                \Lychee\Modules\Log::error(
                    \Lychee\Modules\Database::get(),
                    __METHOD__, __LINE__,
                    "Could not unlink file \"$file\""
                );
            }
        }

        return true;
    }
}
