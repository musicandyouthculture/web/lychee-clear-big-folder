## Lychee Plugin Documentation

https://github.com/DRogueRonin/Lychee/blob/master/docs/Plugins.md

## Install

```
git clone https://gitlab.com/ifmayc/web/lychee-clear-big-folder.git ClearBigFolder
```

Add `ClearBigFolder\ClearBigFolder` to the `lychee_settings` table for `plugins`.

